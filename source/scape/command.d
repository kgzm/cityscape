module scape.command;

import scape.subcommands.jsonToBson;
import scape.subcommands.bench;
import scape.subcommands.findBin;
import scape.subcommands.extractFeatures;
import scape.subcommands.importData;
import scape.subcommands.meshify;

alias Subcommand = void function(string[] args);

void runCommand(string[] args) {
  Subcommand[string] subcommands = [
    "j2b": &cmdJsonToBson,
    "lj": &cmdLoadJson,
    "lb": &cmdLoadBson,
    "find": &cmdFindBin,
    "extract": &cmdExtractFeatures,
    "import": &cmdImport,
    "meshify": &cmdMeshify,
  ];
  string subcommand = args[1];
  subcommands[subcommand](args[1 .. $]);
}
