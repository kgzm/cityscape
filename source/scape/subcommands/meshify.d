module scape.subcommands.meshify;

import scape.feature;
import scape.geometry;
import scape.mesh;

import std.stdio;
import std.array;
import file = std.file;
import vibe.data.json;
import vibe.data.bson;

void cmdMeshify(string[] args) {
  import std.getopt;
  import core.stdc.stdlib : exit;

  struct Options {
    string input;
    string output;
    bool useHeight;
    double searchLatitude;
    double searchLongitude;
    double searchRadius;
  }

  Options options;

  auto result = getopt(
    args,
    std.getopt.config.required,
    "i|input", "Input BSON file.", &options.input,
    std.getopt.config.required,
    "o|output", "Output OBJ file", &options.output,
    "height", "use height in mesh", &options.useHeight
  );
  if (result.helpWanted) {
    defaultGetoptPrinter("Some information about the program.",
                         result.options);
    return;
  }

  immutable(ubyte)[] raw;
  try {
    raw = cast(immutable(ubyte)[]) file.read(options.input);
  }
  catch(Exception e) {
    writefln("Input file error --  %s ", e.msg);
    exit(-1);
  }
  Bson bsonData;
  try {
    // bsonData = new Bson(Bson.Type.object, cast(immutable(ubyte)[]) file.read("dub.bson"));
    bsonData = Bson(Bson.Type.object, raw);
  }
  catch(Exception e) {
    writefln("BSON parse error on %s --  %s ", options.input, e.msg);
    exit(-1);
  }
  auto fs = bsonData["features"];
  Point origin = Point(options.searchLongitude, options.searchLatitude);
  string[] objs;
  ulong offset = 1;
  Mesh mesh;
  double height;
  Feature feature;
  foreach(ulong i, Bson b; fs) {
    feature = Feature.fromBson(b);
    import std.conv;
    if(options.useHeight && !feature.properties["HEIGHTROOF"].isNull) {
      height = feature.properties["HEIGHTROOF"].opt!double * 0.3048;
    } else {
      height = 0.0;
    }
    foreach(Polygon poly; feature.geometry.coordinates) {
      mesh = poly.exterior().fromOrigin(origin).toMesh(height, offset);
      objs ~= mesh.toObj();
      offset = mesh.nextOffset;
    }
  }

  try {
    file.write(options.output, objs.join());
  }
  catch(Exception e) {
    writefln("Output file error -- %s", e.msg);
    exit(-1);
  }
  return;
}
