module scape.subcommands.bench;

import std.stdio;
import file = std.file;
import vibe.data.json;
import vibe.data.bson;


void cmdLoadJson(string[] args) {
  import core.stdc.stdlib : exit;
  string input = args[1];
  string raw;
  try {
    raw = file.readText(input);
  }
  catch(Exception e) {
    writefln("Input file error --  %s ", e.msg);
    exit(-1);
  }
  Json jsonData;
  try {
    jsonData = parseJsonString(raw);
  }
  catch(Exception e) {
    writefln("JSON parse error on %s --  %s ", input, e.msg);
    exit(-1);
  }
  return;
}

void cmdLoadBson(string[] args) {
  import core.stdc.stdlib : exit;
  string input = args[1];

  immutable(ubyte)[] raw;
  try {
    raw = cast(immutable(ubyte)[]) file.read(input);
  }
  catch(Exception e) {
    writefln("Input file error --  %s ", e.msg);
    exit(-1);
  }
  Bson bsonData;
  try {
    // bsonData = new Bson(Bson.Type.object, cast(immutable(ubyte)[]) file.read("dub.bson"));
    bsonData = Bson(Bson.Type.object, raw);
  }
  catch(Exception e) {
    writefln("BSON parse error on %s --  %s ", input, e.msg);
    exit(-1);
  }
  return;
}
