module scape.subcommands.extractFeatures;

import std.stdio;
import file = std.file;
import vibe.data.json;
import vibe.data.bson;

void cmdExtractFeatures(string[] args) {
  import std.getopt;
  import core.stdc.stdlib : exit;

  struct Options {
    string input;
    string output;
  }

  Options options;

  auto result = getopt (args,
                        std.getopt.config.required,
                        "i|input", "Input BSON file.", &options.input,
                        std.getopt.config.required,
                        "o|output", "Output BSON file", &options.output,
                        );
  if (result.helpWanted) {
    defaultGetoptPrinter("Some information about the program.",
                         result.options);
    return;
  }

  immutable(ubyte)[] raw;
  try {
    raw = cast(immutable(ubyte)[]) file.read(options.input);
  }
  catch(Exception e) {
    writefln("Input file error --  %s ", e.msg);
    exit(-1);
  }
  Bson bsonData;
  try {
    // bsonData = new Bson(Bson.Type.object, cast(immutable(ubyte)[]) file.read("dub.bson"));
    bsonData = Bson(Bson.Type.object, raw);
  }
  catch(Exception e) {
    writefln("BSON parse error on %s --  %s ", options.input, e.msg);
    exit(-1);
  }
  auto fs = bsonData["features"];
  Bson[] features;
  foreach(ulong i, feature; fs) {
    import std.conv;
    Bson props = feature["properties"];
    Bson document = Bson([
      "lststatype": Bson(BsonDate.fromString(props["lstmoddate"].opt!string)),
      "lststatype": props["lststatype"],
      "feat_code": props["feat_code"],
      "doitt_id": props["doitt_id"],
      "heightroof": Bson(props["heightroof"].opt!string.to!double),
      "name": props["name"],
      "bin": props["bin"],
      "cnstrct_yr": Bson(props["cnstrct_yr"].opt!string.to!long),
      "shape_len": Bson(props["shape_len"].opt!string.to!double),
      "groundelev": Bson(props["groundelev"].opt!string.to!double),
      "shape_area": Bson(props["shape_area"].opt!string.to!double),
      "geometry": feature["geometry"],
      "type": feature["type"]
    ]);
    features ~= document;
  }

  try {
    file.write(options.output, Bson(features).data);
  }
  catch(Exception e) {
    writefln("Output file error -- %s", e.msg);
    exit(-1);
  }
  return;
}
