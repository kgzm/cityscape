module scape.subcommands.jsonToBson;

import std.stdio;
import file = std.file;
import vibe.data.json;
import vibe.data.bson;

void cmdJsonToBson(string[] args) {
  import std.getopt;
  import core.stdc.stdlib : exit;

  struct Options {
    string input;
    string output;
  }
  Options options;

  auto result = getopt (
    args,
    std.getopt.config.required,
    "i|input", "Input JSON file.", &options.input,
    std.getopt.config.required,
    "o|output", "Output BSON file.", &options.output,
  );
  if (result.helpWanted) {
    defaultGetoptPrinter("Some information about the program.",
                         result.options);
    return;
  }

  string raw;
  try {
    raw = file.readText(options.input);
  }
  catch(Exception e) {
    writefln("Input file error --  %s ", e.msg);
    exit(-1);
  }
  Json jsonData;
  try {
    jsonData = parseJsonString(raw);
  }
  catch(Exception e) {
    writefln("JSON parse error on %s --  %s ", options.input, e.msg);
    exit(-1);
  }
  auto bsonData = Bson.fromJson(jsonData).data;
  try {
    file.write(options.output, bsonData);
  }
  catch(Exception e) {
    writefln("Output file error -- %s", e.msg);
    exit(-1);
  }
}
