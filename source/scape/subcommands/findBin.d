module scape.subcommands.findBin;

import std.stdio;
import file = std.file;
import vibe.data.json;
import vibe.data.bson;

void cmdFindBin(string[] args) {
  import std.getopt;
  import core.stdc.stdlib : exit;

  struct Options {
    string input;
    string bin;
  }

  Options options;

  auto result = getopt (args,
                        std.getopt.config.required,
                        "i|input", "Input JSON file.", &options.input,
                        std.getopt.config.required,
                        "b|bin", "BIN to find", &options.bin,
                        );
  if (result.helpWanted) {
    defaultGetoptPrinter("Some information about the program.",
                         result.options);
    return;
  }

  immutable(ubyte)[] raw;
  try {
    raw = cast(immutable(ubyte)[]) file.read(options.input);
  }
  catch(Exception e) {
    writefln("Input file error --  %s ", e.msg);
    exit(-1);
  }
  Bson bsonData;
  try {
    // bsonData = new Bson(Bson.Type.object, cast(immutable(ubyte)[]) file.read("dub.bson"));
    bsonData = Bson(Bson.Type.object, raw);
  }
  catch(Exception e) {
    writefln("BSON parse error on %s --  %s ", options.input, e.msg);
    exit(-1);
  }
  auto features = bsonData["features"];
  foreach(feature; features) {
    import std.string;
    import std.conv;
    auto maybeBin = feature["properties"]["bin"];
    if(!maybeBin.isNull && maybeBin.get!string == options.bin) {
      Bson props = feature["properties"];
      Bson document = Bson([
        "lststatype": Bson(BsonDate.fromString(props["lstmoddate"].opt!string)),
        "lststatype": props["lststatype"],
        "feat_code": props["feat_code"],
        "doitt_id": props["doitt_id"],
        "heightroof": Bson(props["heightroof"].opt!string.to!double),
        "name": props["name"],
        "bin": props["bin"],
        "cnstrct_yr": Bson(props["cnstrct_yr"].opt!string.to!long),
        "shape_len": Bson(props["shape_len"].opt!string.to!double),
        "groundelev": Bson(props["groundelev"].opt!string.to!double),
        "shape_area": Bson(props["shape_area"].opt!string.to!double),
        "geometry": feature["geometry"],
        "type": feature["type"]
      ]);
      writeln(document.toJson.toPrettyString.detab(2));
      break;
    }
  }
  return;
}
