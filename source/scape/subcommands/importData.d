module scape.subcommands.importData;

import std.stdio;
import file = std.file;
import vibe.data.json;
import vibe.data.bson;
import vibe.db.mongo.mongo;

void cmdImport(string[] args) {
  import std.getopt;
  import core.stdc.stdlib : exit;

  struct Options {
    string input;
  }

  Options options;

  auto result = getopt (args,
                        std.getopt.config.required,
                        "i|input", "Input BSON file.", &options.input,
                        );
  if (result.helpWanted) {
    defaultGetoptPrinter("Some information about the program.",
                         result.options);
    return;
  }

  immutable(ubyte)[] raw;
  try {
    raw = cast(immutable(ubyte)[]) file.read(options.input);
  }
  catch(Exception e) {
    writefln("Input file error --  %s ", e.msg);
    exit(-1);
  }
  Bson features;
  try {
    // bsonData = new Bson(Bson.Type.object, cast(immutable(ubyte)[]) file.read("dub.bson"));
    features = Bson(Bson.Type.array, raw);
  }
  catch(Exception e) {
    writefln("BSON parse error on %s --  %s ", options.input, e.msg);
    exit(-1);
  }
  try {
    MongoClient client = connectMongoDB("127.0.0.1");
    auto coll = client.getCollection("test.features");
    foreach(feature; features) {
      coll.insert(feature);
    }
  }
  catch(Exception e) {
    writefln("Import failed: %s", e.msg);
    exit(-1);
  }
  return;
}
