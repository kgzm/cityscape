module scape.geometry;

import scape.formula;

import vibe.data.bson;
import s2.s2loop;
import s2.s2point;
import s2.s2cap;
import s2.s1angle;
import s2.s2latlng;
import std.algorithm.iteration : map, fold;
import std.array;
import std.math;


// enum double metersPerDegree = 110574;
// enum double degreesPerMeter = 1 / 110574;

enum double metersPerDegree = (2.0 * PI / 360.0);
enum double degreesPerMeter = 1.0 / 110574.0;
enum double radiansPerMeter = 1.0 / 6371.0e3;

struct MultiPolygon {
  //Becomes list of S2 polygons.
  Polygon[] coordinates;
  S2Loop[] toS2() {
    return coordinates.map!(p => p.toS2).array;
  }
  bool intersects(S2Loop loop) {
    return this.toS2().fold!((p, l) => p && loop.intersects(l))(true);
  }
}

struct Polygon {
  //Becomes s2 polygon.
  LinearRing[] coordinates;
  LinearRing exterior() {
    if(this.coordinates.length < 1) {
      return LinearRing.init;
    } else {
      return this.coordinates[0];
    }
  }
  S2Loop toS2() {
    return this.exterior.toS2();
  }
}

struct LinearRing {
  Point[] coordinates;
  this(Point[] cs) {
    coordinates = cs;
  }
  invariant {
    assert(coordinates.length == 0 || coordinates[0] == coordinates[$-1]);
  }

  // Becomes s2 loop.
  S2Loop toS2() {
    S2Loop loop = new S2Loop(coordinates.map!(p => p.toS2).array);
    loop.normalize();
    return loop;
  }
  LinearRing fromOrigin(Point origin) {
    import scape.formula;
    return LinearRing(coordinates.map!(p => toOrigin(p, origin)).array);
  }
}

struct Point {
  double x = 0.0, y = 0.0, z = 0.0;
  alias lon = x;
  alias lat = y;
  S2Point toS2() {
    S2Point p = S2LatLng(S1Angle.fromDegrees(lat),
                         S1Angle.fromDegrees(lon)).toS2Point().normalize;
    return p;
  }
  Point fromOrigin(Point origin) {
    assert(0);
  }
}

T altNaN(T)(T x, T alt) {
  import std.math : isNaN;
  if(x.isNaN) {
    return alt;
  } else {
    return x;
  }
}

T fromBson(T: Point)(Bson b) {
  return Point(
    b[0].get!double,
    b[1].get!double,
  );
}

T fromBson(T)(Bson b) {
  import std.range.primitives;
  alias CoordinateType = ElementType!(typeof(T.coordinates));
  CoordinateType[] cs;
  foreach(c; b) {
    cs ~= fromBson!CoordinateType(c);
  }
  return T(cs);
}

MultiPolygon geometryFromBson(Bson b) {
  if(b["type"].opt!string != "MultiPolygon") {
    import std.stdio;
    writefln("Fatal Error: Only MultiPolygons are suported, got: %s ", b["type"].opt!string);
    assert(false);
  }
  return fromBson!MultiPolygon(b["coordinates"]);
}

S2Point s2PointFromLatLng(double lat, double lng) {
  return S2LatLng(S1Angle.fromDegrees(lat),
                  S1Angle.fromDegrees(lng)).toS2Point();

}

S2Loop s2LoopFromLatLng(S1Angle lat, S1Angle lng, S1Angle radius) {
  return S2Loop.makeRegularLoop(
    S2LatLng(lat, lng).toS2Point(),
    radius,
    64
  );
}

version(unittest) import fluent.asserts;
unittest {
  import std.stdio;
  // Points.
  Point a = {1,2}, b = {1,2}, c = {4,5};
  a.should.equal(b).because("Points that have the same coordinates are equal.");
  a.should.not.equal(c).because("Points that don't have the same coordinates are not equal.");

  // Linear Rings.
  ({
    LinearRing lr = LinearRing([a,b]);
  }).should.not.throwSomething.because("First and last points are equal.");
  ({
    LinearRing lr = LinearRing([a,c]);
  }).should.throwSomething.because("First and last points aren't equal.");
  ({
    LinearRing lr = LinearRing([]);
  }).should.not.throwSomething.because("It should be okay to be empty.");
}
struct SearchLoop {
  S2Loop loop;
  alias loop this;
  this(double lat, double lon, double radius) {
    loop = s2LoopFromLatLng(
      S1Angle.fromDegrees(lat),
      S1Angle.fromDegrees(lon),
      S1Angle.fromRadians(radius * radiansPerMeter)
    );
  }
}
unittest {
  import std.stdio;
  import vibe.data.json;
  Bson b = `{
    "coordinates": [[[
      [-75.98726268863767, 48.69232258559749],
      [-75.98735217812957, 48.69223321475312],
      [-75.98741295967528, 48.69226505040662],
      [-75.98732247021819, 48.69235442129332],
      [-75.98726268863767, 48.69232258559749]
    ]]]
      }`.parseJsonString;
  MultiPolygon mp = fromBson!MultiPolygon(b["coordinates"]);
  //Can't think of a good test for this, oh well.
  S2Loop searchLoop = s2LoopFromLatLng(
    S1Angle.fromDegrees(48.69232258559749),
    S1Angle.fromDegrees(-75.98726268863767),
    S1Angle.fromDegrees(10.0 * degreesPerMeter)
  );
  mp.intersects(searchLoop).should.equal(true);

  S2Loop failingSearchLoop = s2LoopFromLatLng(
    S1Angle.fromDegrees(-10.69232258559749),
    S1Angle.fromDegrees(40.98726268863767),
    S1Angle.fromRadians(1000.0 / 6371.0),
  );
  mp.intersects(failingSearchLoop).should.equal(false)
    .because("It's on the other side of the earth.");

  S2Loop broadSearchLoop = s2LoopFromLatLng(
    S1Angle.fromDegrees(48.69232258559749),
    S1Angle.fromDegrees(-75.98726268863767) - 4.0 * radiansPerMeter,
    S1Angle.fromDegrees(50 * radiansPerMeter)
  );
  mp.intersects(broadSearchLoop).should.equal(true);
}
