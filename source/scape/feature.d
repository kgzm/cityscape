module scape.feature;

import scape.geometry;
import vibe.data.bson;

struct Feature {
  string featureType;
  Bson properties;
  MultiPolygon geometry;
  static Feature fromBson(in Bson b, string featureType = "feature") {
    Feature feature = {
      featureType: featureType,
      properties: b["properties"],
      geometry: geometryFromBson(b["geometry"])
    };
    return feature;
  }
}

version(unittest) import fluent.asserts;
unittest {
  import std.stdio;
  auto b = testdata();
  Feature feature = Feature.fromBson(b);
  Point p1 = feature.geometry.coordinates[0].coordinates[0].coordinates[0];
  Point p2 = Point(-75.98726268863767, 48.69232258559749);
  p1.should.equal(p2).because("Construction shouldn't alter the data.");
}

version(unittest) {
  Bson testdata() {
    auto doc = `
    {
      "type": "Feature",
      "properties": {
        "lstmoddate": "2017-08-22T00:00:00.000Z",
        "lststatype": "Constructed",
        "feat_code": "2100",
        "doitt_id": "444448",
        "heightroof": "33.2",
        "name": null,
        "bin": "3999990",
        "cnstrct_yr": "1955",
        "shape_len": "229.84444881553",
        "groundelev": "48",
        "shape_area": "520.55551573806"
      },
      "geometry": {
        "type": "MultiPolygon",
        "coordinates": [[[
          [-75.98726268863767, 48.69232258559749],
          [-75.98735217812957, 48.69223321475312],
          [-75.98741295967528, 48.69226505040662],
          [-75.98732247021819, 48.69235442129332],
          [-75.98726268863767, 48.69232258559749]
        ]]]
      }
    }
`.parseJsonString;
    return Bson.fromJson(doc);
  }
}
