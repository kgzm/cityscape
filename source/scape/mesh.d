module scape.mesh;

import scape.geometry;

struct Mesh {
  Point[] vertices;
  ulong[][] faces;
  ulong nextOffset;
}


Mesh toMesh(LinearRing ring, double height = 0, long offset = 1) {
  import std.range : iota;
  import std.array;

  Mesh mesh;
  mesh.vertices ~= ring.coordinates[0..$-1];
  mesh.faces ~= iota(offset, offset + ring.coordinates.length - 1).array;

  //If we have it easy.
  if(height == 0) {
    mesh.nextOffset = offset + mesh.vertices.length;
    return mesh;
  }
  ulong[] topface;
  ulong l = ring.coordinates.length - 1;
  foreach(ulong i, Point p; ring.coordinates) {
    if(i == l) break;
    mesh.faces ~= [
      offset + i,               // initial vertex at height 0.
      offset + (i + 1) % l,     // next vertex at height 0.
      offset + (i + 1) % l + l, // vertex above
      offset + i + l,           // vertex above original vertex
    ];
    mesh.vertices ~= Point(p.x, p.y, height);
    topface ~= offset + i + l;
  }
  mesh.nextOffset = offset + mesh.vertices.length;
  mesh.faces ~= topface;
  return mesh;
}

string toObj(Mesh mesh) {
  import std.format;
  import std.algorithm;
  import std.array;
  string output;
  foreach(Point p; mesh.vertices) {
    int precision = 50;
    output ~= format("v %.*f %.*f %.*f\n",
                     precision, p.x,
                     precision, p.y,
                     precision, p.z);
  }
  foreach(ulong[] f; mesh.faces) {
    output ~= "f";
    output ~= f.map!(n => format(" %d", n)).array.join;
    output ~= "\n";
  }
  return output;
}

version(unittest) { import fluent.asserts; }
unittest {
  import std.stdio;
  Point[] points = [
    Point(0,0),
    Point(20,0),
    Point(20,10),
    Point(0,10),
    Point(0,0),
  ];
  LinearRing ring = LinearRing(points);

  Mesh square = ring.toMesh();
  square.vertices.length.should.equal(4);
  square.faces.length.should.equal(1);
  square.nextOffset.should.equal(5);

  Mesh cube = ring.toMesh(30);
  cube.vertices.length.should.equal(8);
  cube.faces.length.should.equal(6);
  cube.nextOffset.should.equal(9);

  Mesh cubeOffset = ring.toMesh(30,5);
  cubeOffset.vertices.length.should.equal(8);
  cubeOffset.faces.length.should.equal(6);
  cubeOffset.nextOffset.should.equal(13);
  // writeln(cubeOffset.toObj());

  ulong[ulong] faceCounts;
  foreach(ulong[] f; cube.faces) {
    foreach(ulong v; f) {
      faceCounts[v] += 1;
    }
  }
  ulong[] properFaceCounts = [3,3,3,3,3,3,3,3];
  faceCounts.values.should.equal(properFaceCounts)
    .because("Every vertex should participate in 3 faces.");
  // writeln(cube.meshToObj());
}
