module scape.formula;

import scape.geometry;

import std.math;
import std.traits: Select, isFloatingPoint;

template isComplex(T) {
  enum bool isComplex = is(T == cfloat) ||
    is(T == cdouble) ||
    is(T == creal);
}

@safe pure nothrow Select!(isFloatingPoint!T || isComplex!T, T, double)
radians(T)(in T x) {
  return x * (PI / 180);
}

@safe pure nothrow Select!(isFloatingPoint!T || isComplex!T, T, double)
degrees(T)(in T x) {
  return x / (PI / 180);
}

T lat(T)(T[] a) {
  return a[0];
}

T lon(T)(T[] a) {
  return a[1];
}

double haversineDistance(in real dth1, in real dph1,
                         in real dth2, in real dph2)
  pure nothrow @nogc {
  enum real R = 6371e3;
  enum real TO_RAD = PI / 180;

  alias imr = immutable real;
  imr ph1d = dph1 - dph2;
  imr ph1 = ph1d * TO_RAD;
  imr th1 = dth1 * TO_RAD;
  imr th2 = dth2 * TO_RAD;

  imr dz = th1.sin - th2.sin;
  imr dx = ph1.cos * th1.cos - th2.cos;
  imr dy = ph1.sin * th1.cos;
  return asin(sqrt(dx ^^ 2 + dy ^^ 2 + dz ^^ 2) / 2) * 2 * R;
}

double distance(Point p1, Point p2) {
  return haversineDistance(p1.lat, p1.lon, p2.lat, p2.lon);
}

double bearingBetween(Point p1, Point p2) {
  import std.math;
  double lat1 = p1.lat, long1 = p1.lon,
    lat2 = p2.lat, long2 = p2.lon;
  double dLon = (long2 - long1);

  double y = sin(dLon) * cos(lat2);
  double x = cos(lat1) * sin(lat2) - sin(lat1)
    * cos(lat2) * cos(dLon);

  double brng = atan2(y, x);

  //Stick with radians for now.
  // brng = (brng + PI) % PI_2;
  brng = PI - brng - PI / 2; // count degrees counter-clockwise - remove to make clockwise
  // brng = brng + (PI * 2) - (PI / 2);
  brng = brng % (PI * 2);
  return brng;
}

double toDegrees(double x) {
  return x * (180 / PI) % 360;
}

double toRadians(double x) {
  return x * (PI / 180);
}

Point toRadians(Point p) {
  with(p) {
    return Point(
      toRadians(x),
      toRadians(y),
      toRadians(z)
    );
  }
}

Point toOrigin(Point origin, Point subject) {
  import std.math;
  double r = distance(origin, subject);
  double theta = bearingBetween(origin, subject);
  return Point(
    r * cos(theta),
    r * sin(theta)
  );
}

version(unittest) import fluent.asserts;
unittest {
  import s2.s1angle;
  import std.stdio;
  Point p1 = Point(-73.92735617812957, 40.69223321475312),
        p2 = Point(-73.92726968863767, 40.69232258559749);
  distance(p1, p2).should.be.approximately(12, 1);
  // bearingBetween(p2, p1).toDegrees().should.be.approximately(45, 4);

  //Write tests for these later.
  Point center = Point(0, 0),
    north = Point(0, 1),
    east = Point(1, 0),
    south = Point(0, -1),
    west = Point(-1, 0),

    northeast = Point(1, 1),
    northwest = Point(-1, 1),
    southwest = Point(-1, -1),
    southeast = Point(1, -1);

  writeln(bearingBetween(center, east).toDegrees().round());
  writeln(bearingBetween(center, north).toDegrees().round());
  writeln(bearingBetween(center, west).toDegrees().round());
  writeln(bearingBetween(center, south).toDegrees().round());
  // writeln(toOrigin(center, east));
  // writeln(toOrigin(center, north));
  // writeln(toOrigin(center, west));
  // writeln(toOrigin(center, south));


  // writeln(toOrigin(east , center));
  // writeln(toOrigin(north, center));
  // writeln(toOrigin(west,  center));
  // writeln(toOrigin(south, center));

  // writeln(toOrigin(east , center));
  // writeln(toOrigin(north, center));
  // writeln(toOrigin(west,  center));
  // writeln(toOrigin(south, center));

  writeln(toOrigin(northeast, center));
  writeln(toOrigin(northwest, center));
  writeln(toOrigin(southwest,  center));
  writeln(toOrigin(southeast, center));

}

// writeln(fromOrigin(p1, p2));
// syr
// cxr
// tyx
// double bearing(T)(T p1, T p2) {
//   double

  
// }
// var y = Math.sin(λ2-λ1) * Math.cos(φ2);
// var x = Math.cos(φ1)*Math.sin(φ2) -
//   Math.sin(φ1)*Math.cos(φ2)*Math.cos(λ2-λ1);
// var brng = Math.atan2(y, x).toDegrees();
